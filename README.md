Course web
==========

Moodle block to include on the dashboard that shows information about a student's main course.

Requirements
------------

- Moodle 3.6 onwards

Installation
------------

1. Extract the plugin in the folder `blocks/courseweb`
2. Navigate to _Site administration > Notifications_ to trigger the upgrade

Setup
-----

The plugin requires two admin settings to be set:

- Course parent category ID
- Course title field

See the plugin's admin settings page for all options.

Testing considerations
----------------------

When testing the plugin you may see the effect of updating the profile field values of a test user. Profile field values are set when the user logs in, so you will have to logout and back in to update the value of your test users' profile fields if there were logged in when you set them.
