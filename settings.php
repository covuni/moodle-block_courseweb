<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if ($ADMIN->fulltree) {

    $settings->add(new \block_courseweb\admin_setting_configcategoryid(
        'block_courseweb/courseparentcategoryid',
        new lang_string('courseparentcategoryid', 'block_courseweb'),
        new lang_string('courseparentcategoryid_desc', 'block_courseweb')
    ));

    $enrolchoices = array_merge(['' => get_string('notapplicable', 'block_courseweb')], array_map(function($plugin) {
        return get_string('pluginname', 'enrol_' . $plugin->get_name());
    }, enrol_get_plugins(true)));

    $settings->add(new admin_setting_configselect(
        'block_courseweb/courseenrolmentmethod',
        new lang_string('courseenrolmentmethod', 'block_courseweb'),
        new lang_string('courseenrolmentmethod_desc', 'block_courseweb'),
        '',
        $enrolchoices
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/fieldcoursetitle',
        new lang_string('fieldcoursetitle', 'block_courseweb'),
        new lang_string('fieldcoursetitle_desc', 'block_courseweb'),
        '',
        PARAM_ALPHANUMEXT
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/fieldcoursesubtitle',
        new lang_string('fieldcoursesubtitle', 'block_courseweb'),
        new lang_string('fieldcoursesubtitle_desc', 'block_courseweb'),
        '',
        PARAM_ALPHANUMEXT
    ));

    $settings->add(new admin_setting_configcheckbox(
        'block_courseweb/linktocourse',
        new lang_string('linktocourse', 'block_courseweb'),
        new lang_string('linktocourse_desc', 'block_courseweb'),
        true
    ));

    $options = [
        '' => get_string('none'),
        '{$a->fullname}' => get_string('coursefullname', 'block_courseweb'),
        '{$a->shortname}' => get_string('courseshortname', 'block_courseweb'),
        '{$a->fullname} - {$a->shortname}' => get_string('coursefullnameandshortname', 'block_courseweb'),
        '{$a->shortname} - {$a->fullname}' => get_string('courseshortnameandfullname', 'block_courseweb'),
    ];
    $settings->add(new admin_setting_configselect(
        'block_courseweb/linkdetails',
        new lang_string('linkdetails', 'block_courseweb'),
        new lang_string('linkdetails_desc', 'block_courseweb'),
        '',
        $options
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/fieldreadinglist',
        new lang_string('fieldreadinglist', 'block_courseweb'),
        new lang_string('fieldreadinglist_desc', 'block_courseweb'),
        '',
        PARAM_ALPHANUMEXT
    ));

    $settings->add(new admin_setting_configtextarea(
        'block_courseweb/validreadinglists',
        new lang_string('validreadinglists', 'block_courseweb'),
        new lang_string('validreadinglists_desc', 'block_courseweb'),
        '',
        PARAM_RAW,
        60,
        4
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/readinglisturl',
        new lang_string('readinglisturl', 'block_courseweb'),
        new lang_string('readinglisturl_desc', 'block_courseweb'),
        '',
        '/(^https?:\/\/.*{code}.*$)|^$/' // Empty, or URL containing {code}.
    ));

    $settings->add(new admin_setting_configcheckbox(
        'block_courseweb/linktoreadinglist',
        new lang_string('linktoreadinglist', 'block_courseweb'),
        new lang_string('linktoreadinglist_desc', 'block_courseweb'),
        true
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/userfilterfield',
        new lang_string('userfilterfield', 'block_courseweb'),
        new lang_string('userfilterfield_desc', 'block_courseweb'),
        '',
        PARAM_ALPHANUMEXT
    ));

    $settings->add(new admin_setting_configtext(
        'block_courseweb/userfiltervalue',
        new lang_string('userfiltervalue', 'block_courseweb'),
        new lang_string('userfiltervalue_desc', 'block_courseweb'),
        '',
        PARAM_RAW_TRIMMED
    ));

}
