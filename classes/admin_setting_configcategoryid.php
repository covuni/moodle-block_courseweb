<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin setting category.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_courseweb;
defined('MOODLE_INTERNAL') || die();

use admin_setting_configtext;
use context_coursecat;

require_once($CFG->libdir . '/adminlib.php');

/**
 * Admin setting category.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class admin_setting_configcategoryid extends admin_setting_configtext {

    /**
     * Constructor.
     *
     * @param string $name The setting's internal name.
     * @param string $visiblename The name of the settings.
     * @param string $description The description.
     */
    public function __construct($name, $visiblename, $description) {
        parent::__construct($name, $visiblename, $description, '', PARAM_INT);
    }

    /**
     * Validate data before storage.
     *
     * @param string The data to validate.
     * @return mixed True, or error string.
     */
    public function validate($data) {
        $validated = parent::validate($data);
        if ($validated !== true) {
            return $validated;
        }

        // The setting is not set.
        if (empty($data)) {
            return true;
        }

        if (context_coursecat::instance($data, IGNORE_MISSING) !== false) {
            return true;
        }

        return get_string('invalidcategoryid', 'block_courseweb');
    }

}
