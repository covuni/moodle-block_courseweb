<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_courseweb\output;
defined('MOODLE_INTERNAL') || die();

/**
 * Renderer.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends \plugin_renderer_base {

    /**
     * Render the course web.
     *
     * @param string $title The title.
     * @param string|null $subtitle The subtitle.
     * @param int[] $courseids The course IDs.
     * @param moodle_url|null $readinglisturl Reading list URLs indexed by course ID.
     * @return string
     */
    public function course_web($title, $subtitle, $courseids, $readinglisturl) {
        $linkdetails = get_config('block_courseweb', 'linkdetails');
        $links = array_reduce($courseids, function($carry, $id) use ($linkdetails) {
            $url = new \moodle_url('/course/view.php', ['id' => $id]);

            $name = get_string('viewcoursepage', 'block_courseweb');
            if (!empty($linkdetails)) {
                $course = get_fast_modinfo($id)->get_course();
                $details = $this->format_link_details($linkdetails, $course);
                if (!empty($details)) {
                    $name = get_string('viewcoursepagewithdetails', 'block_courseweb', $details);
                }
            }

            $carry[] = [
                'name' => $name,
                'url' => $url->out(false),
            ];
            return $carry;
        }, []);
        return $this->render_from_template('block_courseweb/courseweb', [
            'title' => $title,
            'subtitle' => $subtitle,
            'readinglisturl' => !empty($readinglisturl) ? $readinglisturl->out(false) : null,

            'firstcourselink' => reset($links),
            'othercourselinks' => array_slice($links, 1),

            'hasfirstlinks' => !empty($courseids) || !empty($readinglisturl),
            'hasothercourselinks' => count($links) > 1
        ]);
    }

    /**
     * Format link details.
     *
     * @param string $format The format.
     * @param stdClass $course The course.
     * @return string|null
     */
    protected function format_link_details($format, $course) {
        if (empty($format)) {
            return null;
        }

        $search = ['{$a->fullname}', '{$a->shortname}'];
        $replace = [$course->fullname, $course->shortname];

        return str_replace($search, $replace, $format);
    }

}
