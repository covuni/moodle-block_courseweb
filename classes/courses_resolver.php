<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Courses resolver.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_courseweb;
defined('MOODLE_INTERNAL') || die();

use context_coursecat;

/**
 * Courses resolver.
 *
 * The class to resolve the course webs for a given user.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class courses_resolver {

    /** @var int The user ID. */
    protected $userid;
    /** @var int The parent category ID. */
    protected $parentcategoryid;

    /**
     * Constructor.
     *
     * @param int $userid The user ID.
     * @param int $parentcategoryid The parent category ID of the expected course.
     * @param string $enrolmentmethod The enrolment method of the user.
     */
    public function __construct($userid, $parentcategoryid, $enrolmentmethod = null) {
        $this->userid = $userid;
        $this->parentcategoryid = $parentcategoryid;
        $this->enrolmentmethod = !empty($enrolmentmethod) ? $enrolmentmethod : null;
    }

    /**
     * Get the course courses.
     *
     * @return int[]
     */
    public function get_courseids() {
        global $DB;

        $catcontext = context_coursecat::instance($this->parentcategoryid, IGNORE_MISSING);
        if (empty($catcontext)) {
            debugging('The parent category to search the course web in is missing.', DEBUG_DEVELOPER);
            return [];
        }

        $params = [
            'userid' => $this->userid,
            'contextlevel' => CONTEXT_COURSE,
            'active' => ENROL_USER_ACTIVE,
            'enabled' => ENROL_INSTANCE_ENABLED,
            'now1' => time(),
            'now2' => time()
        ];

        // Filter based on parent category.
        $ctxlikesql = $DB->sql_like('ctx.path', ':ctxpath');
        $params['ctxpath'] = $DB->sql_like_escape($catcontext->path . '/') . '%';

        // Filter based on enrolment methods.
        $enrolmethodsql = '1=1';
        if (!empty($this->enrolmentmethod)) {
            $enrolmethodsql = 'e.enrol = :enrolmentmethod';
            $params['enrolmentmethod'] = $this->enrolmentmethod;
        }

        $sql = "SELECT e.courseid
                  FROM {user_enrolments} ue
                  JOIN {enrol} e
                    ON e.id = ue.enrolid
                  JOIN {context} ctx
                    ON ctx.instanceid = e.courseid
                   AND ctx.contextlevel = :contextlevel
                 WHERE ue.userid = :userid
                   AND ue.status = :active
                   AND ue.timestart < :now1
                   AND (ue.timeend = 0 OR ue.timeend > :now2)
                   AND e.status = :enabled
                   AND $ctxlikesql
                   AND $enrolmethodsql";

        return $DB->get_fieldset_sql($sql, $params);
    }

}
