<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['courseenrolmentmethod'] = 'Course enrolment method';
$string['courseenrolmentmethod_desc'] = 'When set, the search for the student\'s course will be limited to those where their enrolment method matches this setting.';
$string['coursefullname'] = 'Course full name';
$string['coursefullnameandshortname'] = 'Course full name - Course short name';
$string['courseparentcategoryid'] = 'Course parent category ID';
$string['courseparentcategoryid_desc'] = 'The category from which the student\'s course will be looked for. In most cases, students should only be enrolled in a single course in that category tree. This is a required setting.';
$string['courseshortname'] = 'Course short name';
$string['courseshortnameandfullname'] = 'Course short name - Course full name';
$string['courseweb:addinstance'] = 'Add a new course web block';
$string['courseweb:myaddinstance'] = 'Add a new course web block to Dashboard';
$string['fieldcoursesubtitle'] = 'Course subtitle field';
$string['fieldcoursesubtitle_desc'] = 'The shortname of the custom profile field that holds the _Course web_ subtitle.';
$string['fieldcoursetitle'] = 'Course title field';
$string['fieldcoursetitle_desc'] = 'The shortname of the custom profile field that holds the _Course web_ title. This is a required settings.';
$string['fieldreadinglist'] = 'Reading list field';
$string['fieldreadinglist_desc'] = 'The shortname of the custom profile field that holds the reading list code.';
$string['invalidcategoryid'] = 'Invalid category ID';
$string['linkdetails'] = 'Link course details';
$string['linkdetails_desc'] = 'The information about the course to include within the link. This is especially useful when students belong to multiple course webs, as without details the links to the various courses will all look identical.';
$string['linktocourse'] = 'Link to course';
$string['linktocourse_desc'] = 'Whether the block should display a link to go to the course.';
$string['linktoreadinglist'] = 'Link to reading list';
$string['linktoreadinglist_desc'] = 'Whether the block should display a link to the reading list.';
$string['notapplicable'] = 'N/A';
$string['placeholderforblockeditors'] = '_This block will display their \'Course web\' to students._';
$string['pluginname'] = 'Course web';
$string['pluginnotconfigured'] = '_The plugin is not configured, please check its [settings]({$a->settingsurl})._';
$string['readinglisturl'] = 'Reading list URL';
$string['readinglisturl_desc'] = 'This is the URL to use when constructing the URL to the reading list. The placeholder `{code}` must be present in the URL and will be substituted by the reading list code. Example: `https://example.com/read.php?list={code}&expand=true` will become `https://example.com/read.php?list=ADR007_S1&expand=true`.';
$string['userfilterfield'] = 'User filter field';
$string['userfilterfield_desc'] = 'The shortname of the custom profile field to use for filtering users (e.g. staff vs. students). This is to be used in combination with the _User filter value_ setting. Leave this empty to disable filtering.';
$string['userfiltervalue'] = 'User filter value';
$string['userfiltervalue_desc'] = 'The value that must be found in the user profile field defined in _User filter field_ for the user to be displayed the block. ';
$string['validreadinglists'] = 'Valid reading list codes ';
$string['validreadinglists_desc'] = 'The list of valid reading list codes. If the value from the _Readling list field_ is not found in this list, the user will not be shown the reading list. There must only be one code per line.';
$string['viewcoursepage'] = 'View Course Page';
$string['viewcoursepagewithdetails'] = 'View Course Page ({$a})';
$string['viewcoursereadinglist'] = 'View Course Reading list';
