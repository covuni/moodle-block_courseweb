<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block class.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Block class.
 *
 * @package    block_courseweb
 * @copyright  2019 Coventry University
 * @author     Frédéric Massart <fred@branchup.tech>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_courseweb extends block_base {

    /**
     * Init.
     *
     * @return void
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_courseweb');
    }

    /**
     * Applicable formats.
     *
     * @return array
     */
    public function applicable_formats() {
        return ['my' => true];
    }

    /**
     * Get content.
     *
     * @return stdClass
     */
    public function get_content() {
        global $USER;

        if ($this->content !== null) {
            return $this->content;
        }

        $this->content = (object) ['text' => '', 'footer' => '' ];

        $isediting = $this->page->user_is_editing();
        $canedit = $this->user_can_edit();
        $canseenotice = $isediting && $canedit;

        // Leave early when the plugin is not configured.
        if (!$this->is_plugin_configured()) {
            if ($canseenotice) {
                $settingsurl = new moodle_url('/admin/settings.php', ['section' => 'blocksettingcourseweb']);
                $this->content->text = markdown_to_html(
                    get_string('pluginnotconfigured', 'block_courseweb', (object) [
                        'settingsurl' => $settingsurl->out(false)
                    ])
                );
            }
            return $this->content;
        }

        // Check whether the user matches the required filter.
        if (!$this->does_user_match_filter()) {
            if ($canseenotice) {
                $this->content->text = markdown_to_html(get_string('placeholderforblockeditors', 'block_courseweb'));
            }
            return $this->content;
        }

        // Get the course title and return if there aren't any.
        $coursetitle = $this->get_course_web_title();
        if (empty($coursetitle)) {
            if ($canseenotice) {
                $this->content->text = markdown_to_html(get_string('placeholderforblockeditors', 'block_courseweb'));
            }
            return $this->content;
        }

        // Find the courses IDs.
        $resolver = new \block_courseweb\courses_resolver(
            $USER->id,
            (int) get_config('block_courseweb', 'courseparentcategoryid'),
            get_config('block_courseweb', 'courseenrolmentmethod')
        );

        $courseids = [];
        $readinglisturl = null;
        if (get_config('block_courseweb', 'linktocourse')) {
            $courseids = $resolver->get_courseids();
        }
        if (get_config('block_courseweb', 'linktoreadinglist')) {
            $readinglisturl = $this->get_reading_list_url();
        }

        // Render!
        $renderer = $this->page->get_renderer('block_courseweb');
        $this->content->text = $renderer->course_web($coursetitle, $this->get_course_web_subtitle(), $courseids, $readinglisturl);

        return $this->content;
    }

    /**
     * Whether the plugin has settings.
     *
     * @return bool
     */
    public function has_config() {
        return true;
    }

    /**
     * Does the user qualify to be shown the block?
     *
     * @return bool
     */
    protected function does_user_match_filter() {
        global $USER;

        $filterfield = get_config('block_courseweb', 'userfilterfield');
        $filtervalue = get_config('block_courseweb', 'userfiltervalue');

        // We don't filter anybody.
        if (empty($filterfield)) {
            return true;
        }

        if (!isset($USER->profile)) {
            // That is odd, the profile fields should be available, never mind we skip the user.
            debugging('User profile fields were not found in the user object, this is unexpected.', DEBUG_DEVELOPER);
            return false;

        } else if (!isset($USER->profile[$filterfield])) {
            // The field doesn't exist on the user, or is null, bail.
            return false;
        }

        // We don't use === comparison as the types are uncertain at this point.
        return $USER->profile[$filterfield] == $filtervalue;
    }

    /**
     * Get the course web title.
     *
     * @return string|null
     */
    protected function get_course_web_title() {
        global $USER;

        if (!isset($USER->profile)) {
            debugging('User profile fields were not found in the user object, this is unexpected.', DEBUG_DEVELOPER);
            return null;
        }

        $field = get_config('block_courseweb', 'fieldcoursetitle');
        if (!isset($USER->profile[$field])) {
            return null;
        }

        return $USER->profile[$field];
    }

    /**
     * Get the course web sub title.
     *
     * @return string|null
     */
    protected function get_course_web_subtitle() {
        global $USER;

        $field = get_config('block_courseweb', 'fieldcoursesubtitle');
        if (empty($field)) {
            return null;

        } else if (!isset($USER->profile) || !isset($USER->profile[$field])) {
            return null;
        }

        return $USER->profile[$field];
    }

    /**
     * Get reading list code.
     *
     * @return string|null
     */
    protected function get_reading_list_code() {
        global $USER;

        $field = get_config('block_courseweb', 'fieldreadinglist');
        if (empty($field)) {
            return null;

        } else if (!isset($USER->profile) || !isset($USER->profile[$field])) {
            return null;
        }

        // Validate that the code appears in the valid codes.
        $code = $USER->profile[$field];
        $validfields = array_flip(array_map('trim', explode("\n", get_config('block_courseweb', 'validreadinglists'))));
        if (!array_key_exists($code, $validfields)) {
            return null;
        }

        return $code;
    }

    /**
     * Get the reading list URL.
     *
     * @return moodle_url There should only be one entry.
     */
    protected function get_reading_list_url() {
        global $DB;

        $urltemplate = get_config('block_courseweb', 'readinglisturl');
        $readinglist = $this->get_reading_list_code();
        if (empty($urltemplate) || empty($readinglist)) {
            return null;
        }

        return new moodle_url(str_replace('{code}', urlencode($readinglist), $urltemplate));
    }

    /**
     * Check whether the plugin was configured.
     *
     * @return bool
     */
    protected function is_plugin_configured() {
        $title = get_config('block_courseweb', 'fieldcoursetitle');
        $catid = get_config('block_courseweb', 'courseparentcategoryid');
        return !empty($title) && !empty($catid);
    }

    /**
     * Whether to hide the header.
     *
     * @return bool
     */
    public function hide_header() {
        return true;
    }

}
